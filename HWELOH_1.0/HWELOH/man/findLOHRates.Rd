\name{findLOHRates}
\alias{findLOHRates}
\title{
Find LOH Rates
}
\description{
Returns an estimate of the rate of loss of heterozygosity at multiple loci, with each loci's genotypes given as a row in a matrix.
}
\usage{
findLOHRates(genotypes)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{genotypes}{
a matrix of numeric genotypes (one row per locus) with 0 and 2 representing homozygous genotypes, 1 representing heterozygous genotypes and -1 representing a no call
}
}
\details{
Given a matric of genotypes with all genotypes in a row corresponding to the same locus, this function returns a maximum likelihood estimate of LOH rate for a tumor sample at each locus. This computation depends on the assumptions that either allele is equally likely to have been lost and that the corresponding normal was in Hardy-Weinberg equilibrium.
}
\value{
returns numeric vector of decimal values indicating the estimated percentage of individuals which have experienced a loss event at each locus for which genotypes were given
}
\note{
This function is faster than simply applying \code{\link{findLOHRate}} to the matrix, as this function stores  and can re-use each computed probability of observing a given set of genotypes, while \code{\link{findLOHRate}} will recompute probabilities if they are needed multiple times.
}
\references{
Wilkins K and LaFramboise T. Losing Balance: Hardy-Weinberg Disequilibrium as a Marker for Recurrent Loss-of-Heterozygosity in Cancer, submitted (2011).
}
\author{
Thomas LaFramboise and Katherine Wilkins
}

\seealso{
\code{\link{findGenes}}, \code{\link{findLOHRegions}}, \code{\link{findLOHRate}}
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function(genotypes){
n<-dim(genotypes)[2]
TL<-list()
##generate matrices to store p-values (one matrix per possible number of missing samples)
for(k in 0:(n-1)){


M<-matrix(0,nrow=n+1-k,ncol=n+1-k)

TL[[k +1]]<-M}
findLOHRate <-
function(genotypes){
#################################################
#modified SNPHWE function which gets probability of given genotypes, instead of p-value
SNPHWE2 <- function(obs_hets, obs_hom1, obs_hom2){

   if (obs_hom1 < 0 || obs_hom2 < 0 || obs_hets < 0)
      return(-1.0)

   # total number of genotypes
   N <- obs_hom1 + obs_hom2 + obs_hets
   
   # rare homozygotes, common homozygotes
   obs_homr <- min(obs_hom1, obs_hom2)
   obs_homc <- max(obs_hom1, obs_hom2)

   # number of rare allele copies
   rare  <- obs_homr * 2 + obs_hets

   # Initialize probability array
   probs <- rep(0, 1 + rare)

   # Find midpoint of the distribution
   mid <- floor(rare * ( 2 * N - rare) / (2 * N))
   if ( (mid \%\% 2) != (rare \%\% 2) ) mid <- mid + 1

   probs[mid + 1] <- 1.0
   mysum <- 1.0

   # Calculate probablities from midpoint down 
   curr_hets <- mid
   curr_homr <- (rare - mid) / 2
   curr_homc <- N - curr_hets - curr_homr

   while ( curr_hets >=  2)
      {
      probs[curr_hets - 1]  <- probs[curr_hets + 1] * curr_hets * (curr_hets - 1.0) / (4.0 * (curr_homr + 1.0)  * (curr_homc + 1.0))
      mysum <- mysum + probs[curr_hets - 1]

      # 2 fewer heterozygotes -> add 1 rare homozygote, 1 common homozygote
      curr_hets <- curr_hets - 2
      curr_homr <- curr_homr + 1
      curr_homc <- curr_homc + 1
      }    

   # Calculate probabilities from midpoint up
   curr_hets <- mid
   curr_homr <- (rare - mid) / 2
   curr_homc <- N - curr_hets - curr_homr
   
   while ( curr_hets <= rare - 2)
      {
      probs[curr_hets + 3] <- probs[curr_hets + 1] * 4.0 * curr_homr * curr_homc / ((curr_hets + 2.0) * (curr_hets + 1.0))
      mysum <- mysum + probs[curr_hets + 3]
         
      # add 2 heterozygotes -> subtract 1 rare homozygtote, 1 common homozygote
      curr_hets <- curr_hets + 2
      curr_homr <- curr_homr - 1
      curr_homc <- curr_homc - 1
      }    
 
    # P-value calculation
    target <- probs[obs_hets+1]/mysum

    #plo <- min(1.0, sum(probs[1:obs_hets + 1]) / mysum)

    #phi <- min(1.0, sum(probs[obs_hets + 1: rare + 1]) / mysum)

    # This assignment is the last statement in the fuction to ensure 
    # that it is used as the return value
    #p <- min(1.0, sum(probs[probs <= target])/ mysum)
    }
####################################################


na<-length(which(is.na(genotypes)))+length(which(genotypes == -1))
if(na == length(genotypes)){
return(0)
  }
n<-length(genotypes)-na
h<-sum(genotypes==1)
m<-(2*min(c(sum(genotypes == 0), sum(genotypes == 2)))) + h

## y = hypothesized number of hets before loss, h = observed hets, n = number of samples, 2n = the number of alleles, 
## L = number of samples having undergone LOH, m = minor allele frequency
findProb<-function(y, n, h, L, m){
 hom1<-c((m-y)/2)
 M<-TL[[na +1]]
 prob<-M[y+1, hom1+1]
 if(prob == FALSE){
 prob<-SNPHWE2(y, hom1, n-hom1-y)
 M[y+1, hom1+1]<-prob
 }
 dhyper((y-h), y, (n-y), L)*prob->prob
 return(prob)
  }


chooseL<-function(L, n, h, m){
 y<-seq(h,min(h+L,m),by=2)
 sapply(y, findProb, n = n, h = h, L = L, m = m)->probs
 sum(probs)->prob
 return(prob)
  }

## check the probability of a sample having the observed number of hets for each possible number of samples which could have undergone LOH
## (from 0 to all the samples which are not hets) and calls the LOH percent as being that which produces the observed genotypes with
## the highest probability
L<-c(0:(n-h))
sapply(L, chooseL, n = n, h = h, m = m)->probs
L[which.max(probs)]->bestL
return(bestL/n)
  }
apply(genotypes, 1, findLOHRate)->LOHRates
return(LOHRates)
  }
}