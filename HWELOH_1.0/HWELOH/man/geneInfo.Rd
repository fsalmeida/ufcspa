\name{geneInfo}
\alias{geneInfo}
\docType{data}
\title{
Gene Info File
}
\description{
This dataset maps genes to their genomic location.  It also indicates the direction in which the gene is transcribed.
}
\usage{data(geneInfo)}
\format{
SNPinfo is a dataframe with rows corresponding to genes. Columns are as follows: 
\itemize{
\item Chromosome gives the chromosome on which the gene is found
\item Start gives the location of the beginning of the transcribed region in base positions from the beginning of the chromosome
\item End gives the location of the end of the transcribed region in base positions from the beginning of the chromosome
\item Direction indicates the direction in which the gene is transcribed (1 indicates in the forward direction, -1 in the reverse)
}
}
\source{
UCSC Genome Browser, build 18 (http://genome.ucsc.edu/).
}
\examples{
data(geneInfo)
}
\keyword{datasets}
