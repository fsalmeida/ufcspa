\name{findLOHRegions}
\alias{findLOHRegions}
\title{
Find LOH Regions
}
\description{
Given vectors of LOH rate estimates and p-values for neighboring SNPs, this function calls peaks in the LOH rates based on an approximation of the first and second derivatives of the smoothed LOH rates.  SNPs in peaks passing specified cutoffs in LOH rate and p-value are returned.
}
\usage{
findLOHRegions(LOHRates, Pvals, PvalCutoff = 1e-06, passes = 20, LOHThre = 0.2)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{LOHRates}{
A numeric vector of LOH rates (estimates of the percentage of samples which have experienced LOH at a given locus) which can be obtained using the function \code{\link{findLOHRate}}.  The names of the vectors should be the rsIDs of the SNP for which an LOH estimate is given.
}
  \item{Pvals}{
A numeric vector of Hardy-Weinberg equilibrium p-values which can be obtained using the function SNPHWE (found at http://www.sph.umich.edu/csg/abecasis/Exact/snp_hwe.r). The names of the vectors should be the rsIDs of the SNP for which a p-value is given.
}
  \item{PvalCutoff}{
Each called peak must contain a SNP with a p-value below this value to be reported as a region where a loss event occured. 
}
  \item{passes}{
Number of smoothing passes to be completed with the kernal average smoother before calling peaks.
}
  \item{LOHThre}{
Each called peak must contain a SNP with an estimated LOH rate greater than this percentage to be reported as a region where a loss event occured. 
}
}
\value{
A vector of rsIDs, identifying SNPs in regions which have passed both the estimated LOH rate and p-value cutoff given above.
}
\note{
The vector of LOH rates and the vector of p-values passed to this function are assumed to have the values for the same SNP at the same index.  It does not make sense to pass the function vector of non-contiguous SNPs, as smoothing assumes that SNPs which are next to each other in the vectors are also next to each other in the human genome.
}
\references{
Wilkins K and LaFramboise T. Losing Balance: Hardy-Weinberg Disequilibrium as a Marker for Recurrent Loss-of-Heterozygosity in Cancer, submitted (2011).

Wigginton JE, Cutler DJ and Abecasis GR (2005). A note on exact tests of Hardy-Weinberg equilibrium. Am J Hum Genet 76:887-93
}
\author{
Thomas LaFramboise and Katherine Wilkins
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
\code{\link{findLOHRate}}, \code{\link{findGenes}}, \code{\link{findLOHRates}}
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function(LOHRates, Pvals, PvalCutoff = .000001, passes = 20, LOHThre = .2){
library(numDeriv)
smoothLOH<-function(LOHRate, passes){
##kernel average smoother
for(k in 1:passes){
print(paste("smoothing pass", k))
LOHRate[1]<-mean(LOHRate[1:2])
for(j in 2:(length(LOHRate) - 1)){
SNPs<-LOHRate[(j-1):(j+1)]
LOHRate[j]<-mean(SNPs)
  }
LOHRate[length(LOHRate)]<-mean(LOHRate[(length(LOHRate) - 1):length(LOHRate)])
  }
return(LOHRate)
  }

##function which selects peaks using an approximate derivative of the smoothed LOH rates
findLOHRegions2<-function(LOHRates, passes = passes, LOHThre = LOHThre){
LOHRate<-LOHRates[which(!is.na(LOHRates))]
rsIDs<-row.names(LOHRate)
vect<-smoothLOH(LOHRate, passes)
upLim<-LOHThre

##initially assign every data point NoLOH (0)
class<-rep(0,length(vect))

## fit line to data
funcgrad<-splinefun(vect)

## get derivative
gradient<-grad(funcgrad, 1:length(vect))
ind<-c()
##find potential optima in 1st deriv, because these could be zeros in the 2nd deriv
##meaning concavity might change at these points
ind<-sapply(c(2:(length(gradient)-1)), function(i){if((gradient[i]> gradient[i+1] && gradient[i]> gradient[i-1]) || (gradient[i]< gradient[i+1] && gradient[i]< gradient[i-1])){return(i)}})
temp<-vector(length = 0)
for(i in 1:(length(ind))){
if(!is.null(ind[[i]][1])){
temp<-c(temp, ind[[i]][1])
  }
  }
ind<-temp

curr<-1
state<-"non-seen"
i<-curr+1
##selects peak between first slope of one sign to last slope of the opposite sign
while(curr<length(ind)){
while(i<length(ind)&& ( ( gradient[ind[curr]]*gradient[ind[i]]>0 && state=="non-seen") || gradient[ind[curr]]*gradient[ind[i]]<0)){
if(gradient[ind[curr]]*gradient[ind[i]]<0){
state<-"seen"
  }
i<-i+1
  }
## if we stopped because we reached the end of the region, not just the end of the data
if(i!=length(ind)){
i<-i-1
  }
state<-"non-seen"
## select the region
reg<-vect[ind[curr]:ind[i]]
##if this is a max (slope of 1st point is +)
if(gradient[ind[curr]]>0){
if(any(reg>upLim)){
class[ind[curr]:ind[i]]<-c(1)
curr<-i
                        i<-curr+1
  }
else{
curr<-curr+1
  }
  }
else{curr<-curr+1}
  }


gc()
# Now as a post-processing step, check if there are normal regions which are squeezed between two 
# anomaly regions and small enough to be turned into the same region

breaks<-which(class[-1]!=class[-length(class)]) 
if(length(breaks)>0){
start<-breaks[1]+1
for(i in 2:(length(breaks)-1)){
end<-breaks[i]
if(class[start]==2){
if(class[end+1]==class[start-1] && class[end+1]!=2 && (end-start)<20){
class[start:end]<-class[end+1]
  }
  }
start<-end+1
  }
  }
rm(vect,gradient)
gc()
names(class)<-rsIDs
return(class)

  }

##function which selects regions containing a p-value which passes the given cutoff
selectLOHRegions<-function(class, Pvals){
Pvals[which(is.na(Pvals))]<-c(1)
which(class == 1)->LOH
region<-LOH[1]
regions<-vector(length = 0)
i<-c(1)
while(i < length(LOH)){
if((LOH[i+1] - LOH[i]) == 1){
region<-c(region, LOH[i+1])
  }
else{
checkPvals(region, Pvals, PvalCutoff)->pass
if(pass){regions<-c(regions, region)}
region<-LOH[i+1]
  }
i<-c(i+1)
  }
return(names(Pvals)[regions])
  }

##function which checks if any p-values in a given region pass the specified cutoff
checkPvals<-function(region, Pvals, PvalCutoff = PvalCutoff){
return(any(Pvals[region] <= PvalCutoff))
  }
findLOHRegions2(LOHRates, passes, LOHThre)->class
selectLOHRegions(class, Pvals)->rsIDs
return(rsIDs)
  }
}
